//%attributes = {"invisible":true}
  // ----------------------------------------------------
  // Project Method: SLCT_LesserOf ( Number 1 {; Number 2 {; Number N }}) --> Loweset number

  // Allows you to all the Header Titles (max 22) at once

  // Access: Shared

  // Parameters:
  //   $1 - $N : Real 

  // Returns:
  //   $0 : Real 

  // Created by Wayne Stewart (28/07/2020)

  //     wayne@4dsupport.guru
  // ----------------------------------------------------

If (False:C215)
	C_REAL:C285(SLCT_LesserOf ;${1};$0)
End if 

C_REAL:C285(${1};$0)
C_LONGINT:C283($Par_i;$MaxPar_i)
$0:=MAXTEXTLENBEFOREV11:K35:3
$0:=MAXLONG:K35:2
$MaxPar_i:=Count parameters:C259
For ($Par_i;1;$MaxPar_i)
	If (${$Par_i}<$0)
		$0:=${$Par_i}
	End if 
End for 
