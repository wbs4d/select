//%attributes = {"invisible":true}
  // ----------------------------------------------------
  // Project Method: SLCT_Reset

  // Resets the Slct object

  // Access: Private

  // Created by Wayne Stewart (2020-07-01T14:00:00Z)

  //     wayne@4dsupport.guru
  // ----------------------------------------------------

  //
Slct:=New object:C1471

Slct.initialised:=True:C214
Slct.hideControls:=True:C214
  // Slct_Config:=New object
Slct.HorizontalScrollBar:=False:C215
Slct.windowWidth:=0  // This will force default size
Slct.windowHeight:=0  //  when tested later
Slct.buttonFlags:=0
Slct.childPointer:=Null:C1517
Slct.tablePointer:=Null:C1517
Slct.mainText:=""
Slct.explanatoryText:=""
Slct.widths:=""
Slct.Align:=New object:C1471("H";New object:C1471;"V";New object:C1471)

If (Application type:C494=4D Remote mode:K5:5)
	Slct.messageRecordLimit:=Storage:C1525.k.remoteModeLimit
	Slct.messageText:=Storage:C1525.k.remoteModeMessage
Else 
	Slct.messageRecordLimit:=Storage:C1525.k.localModeLimit
	Slct.messageText:=Storage:C1525.k.localModeMessage
End if 

ARRAY TEXT:C222(SLCT_Format_at;Storage:C1525.k.maxColumns)
ARRAY TEXT:C222(SLCT_HeaderTitles_at;Storage:C1525.k.maxColumns)



ARRAY POINTER:C280(SLCT_Fields_aptr;0)
ARRAY POINTER:C280(SLCT_Arrays_aptr;0)
ARRAY POINTER:C280(SLCT_ColHeaders_aptr;0)
ARRAY TEXT:C222(SLCT_ColumnNames_at;0)
ARRAY TEXT:C222(SLCT_HeaderNames_at;0)

SLCT_Text1_t:=""
SLCT_Text2_t:=""

ARRAY LONGINT:C221(SLCT_SortArray_ai;0)

ARRAY TEXT:C222(SLCT_Text_at1;0)
ARRAY TEXT:C222(SLCT_Text_at2;0)
ARRAY TEXT:C222(SLCT_Text_at3;0)
ARRAY TEXT:C222(SLCT_Text_at4;0)
ARRAY TEXT:C222(SLCT_Text_at5;0)
ARRAY TEXT:C222(SLCT_Text_at6;0)

ARRAY DATE:C224(SLCT_Date_ad1;0)
ARRAY DATE:C224(SLCT_Date_ad2;0)
ARRAY DATE:C224(SLCT_Date_ad3;0)

ARRAY REAL:C219(SLCT_Real_ar1;0)
ARRAY REAL:C219(SLCT_Real_ar2;0)
ARRAY REAL:C219(SLCT_Real_ar3;0)

ARRAY LONGINT:C221(SLCT_Long_ai1;0)
ARRAY LONGINT:C221(SLCT_Long_ai2;0)
ARRAY LONGINT:C221(SLCT_Long_ai3;0)

ARRAY BOOLEAN:C223(SLCT_Bool_ab1;0)
ARRAY BOOLEAN:C223(SLCT_Bool_ab2;0)
ARRAY BOOLEAN:C223(SLCT_Bool_ab3;0)

ARRAY PICTURE:C279(SLCT_Pict_apic1;0)
ARRAY PICTURE:C279(SLCT_Pict_apic2;0)
ARRAY PICTURE:C279(SLCT_Pict_apic3;0)

ARRAY LONGINT:C221(SLCT_Index_ai;0)
ARRAY BOOLEAN:C223(SLCT_ListBox_ab;0)