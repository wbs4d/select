//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: SLCT_Alignment (Column Number {; Alignment}) ->  Alignment

  // Column Number

  // Access: Shared

  // Parameters:
  //   $1 : Longint : Column Number
  //   $2 : Longint : Alignment (use constant)

  // Created by Wayne Stewart (2020-10-06T13:00:00Z)

  //     wayne@4dsupport.guru
  // ----------------------------------------------------

C_OBJECT:C1216($0)
C_LONGINT:C283($1;$2)

C_LONGINT:C283($HorizontalAlign_i;$VerticalAlign_i)
C_TEXT:C284($Key_t)

If (False:C215)
	C_OBJECT:C1216(SLCT_Alignment ;$0)
	C_LONGINT:C283(SLCT_Alignment ;$1;$2)
End if 


SLCT_Init 

$Key_t:="c"+String:C10($1)

If (Count parameters:C259=2)
	
	Case of 
		: ($2=SLCT Align H Centre)
			Slct.Align.H[$Key_t]:=Align center:K42:3
			
		: ($2=SLCT Align H Default)
			Slct.Align.H[$Key_t]:=Align default:K42:1
			
		: ($2=SLCT Align H Left)
			Slct.Align.H[$Key_t]:=Align left:K42:2
			
		: ($2=SLCT Align H Right)
			Slct.Align.H[$Key_t]:=Align right:K42:4
			
		: ($2=SLCT Align V Top)
			Slct.Align.V[$Key_t]:=Align top:K42:5
			
		: ($2=SLCT Align V Centre)
			Slct.Align.V[$Key_t]:=Align center:K42:3
			
		: ($2=SLCT Align V Top)
			Slct.Align.V[$Key_t]:=Align bottom:K42:6
			
		: ($2=SLCT Align V Top)
			Slct.Align.V[$Key_t]:=Align default:K42:1
			
	End case 
	
End if 

If (Slct.Align.H[$Key_t]#Null:C1517)
	$HorizontalAlign_i:=Slct.Align.H[$Key_t]
Else 
	$HorizontalAlign_i:=Align default:K42:1
End if 

If (Slct.Align.V[$Key_t]#Null:C1517)
	$VerticalAlign_i:=Slct.Align.V[$Key_t]
Else 
	$VerticalAlign_i:=Align default:K42:1
End if 

$0:=New object:C1471("H";$HorizontalAlign_i;"V";$VerticalAlign_i)