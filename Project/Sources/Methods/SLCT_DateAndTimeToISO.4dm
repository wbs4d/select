//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: SLCT_DateAndTimeToISO (date{; time}) --> Text

// Returns an ISO 8601 formatted date or date-time value.
// If only a date is supplied, a calendar date is returned: YYYY-MM-DD
// If both date and time are supplied, a date-time value is returned: YYYY-MM-DDThh:mm:ss
// If both are passed but the date is !00/00/00! then only the time is returned: hh:mm:ss

// For more information:
//   <http://www.iso.org/iso/en/prods-services/popstds/datesandtime.html>

// Access: Shared

// Parameters: 
//   $1 : Date : A date
//   $2 : Time : A time (optional)

// Returns: 
//   $0 : Text : The date-time stamp

// Created by Dave Batton on Jan 7, 2005
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------

C_TEXT:C284($0;$dateTimeStamp_t)
C_DATE:C307($1;$date_d)
C_TIME:C306($2)
C_TEXT:C284($time_t;$hours_t;$minutes_t;$seconds_t)

$date_d:=$1

$dateTimeStamp_t:=String:C10(Year of:C25($date_d);"0000")
$dateTimeStamp_t:=$dateTimeStamp_t+"-"+String:C10(Month of:C24($date_d);"00")  //month
$dateTimeStamp_t:=$dateTimeStamp_t+"-"+String:C10(Day of:C23($date_d);"00")  //day

If (Count parameters:C259>=2)
	If ($date_d=!00-00-00!)
		$dateTimeStamp_t:=""
	Else 
		$dateTimeStamp_t:=$dateTimeStamp_t+"T"
	End if 
	
	$time_t:=String:C10($2;HH MM SS:K7:1)
	$hours_t:=Substring:C12($time_t;1;2)
	$minutes_t:=Substring:C12($time_t;4;2)
	$seconds_t:=Substring:C12($time_t;7;2)
	$dateTimeStamp_t:=$dateTimeStamp_t+$hours_t+":"+$minutes_t+":"+$seconds_t
End if 
//
$0:=$dateTimeStamp_t