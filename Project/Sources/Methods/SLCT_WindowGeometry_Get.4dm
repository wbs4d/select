//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: SLCT_WindowGeometry_Get -> Object
C_OBJECT:C1216($0)

If (False:C215)
	C_OBJECT:C1216(SLCT_WindowGeometry_Get ;$0)
End if 

  // Retrieves the SLCT Window Geometry

  // Returns

  // Created by Wayne Stewart (2020-07-06T14:00:00Z)
  //     wayne@4dsupport.guru
  // ----------------------------------------------------

SLCT_Init 

$0:=slctConfig

slctConfig:=Null:C1517